//
//  ViewController.swift
//  flipFromLeftAnimations
//
//  Created by KMSOFT on 22/05/17.
//  Copyright © 2017 VKGraphics. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var listContainerView: UIView!
    @IBOutlet weak var mapContainerView: UIView!
    
    var flag:Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func flipFromLeftButtonClick(_ sender: UIButton) {
        if flag == true {
            print("Left")
            UIView.transition(with: self.mapContainerView, duration: 1.0, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: {
                UIView.transition (with: self.listContainerView, duration: 1.0, options: .transitionFlipFromLeft, animations: {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {                        self.listContainerView.isHidden = false;
                        self.mapContainerView.isHidden = true;
                    })
                })
            })
            self.flag = false
        }
        else{
            self.flag = true
            print("Right")
            UIView.transition(with: self.listContainerView, duration: 1.0, options: .transitionFlipFromRight, animations: {
                UIView.transition (with: self.mapContainerView, duration: 1.0, options: UIViewAnimationOptions.transitionFlipFromRight, animations: {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        self.mapContainerView.isHidden = false;
                        self.listContainerView.isHidden = true;
                    })
                })
            })
        }
    }
}

